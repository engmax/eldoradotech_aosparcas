package com.example.hellogps;

import android.app.Activity;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.TextView;
import android.os.Bundle;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class HelloGPS extends AppCompatActivity
{
	private LocationManager locationManager = null;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
		setContentView(R.layout.hellogps);
		locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        // TextView  tv = new TextView(this);
		TextView  latitude = (TextView) findViewById(R.id.textView2);
		TextView  longitude = (TextView) findViewById(R.id.textView4);

        // tv.setText( "Hello GPS!" );
        // setContentView(tv);

		LocationListener locationListener = new LocationListener() {
			public void onLocationChanged(Location location) {
				latitude.setText(String.format("%.3f", location.getLatitude()));
				longitude.setText(String.format("%.3f", location.getLongitude()));
			}
			public void onStatusChanged(String provider, int status, Bundle extras) { }
			public void onProviderEnabled(String provider) { }
			public void onProviderDisabled(String provider) { }
		};

		try {
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0L, 0F, locationListener);
			// setContentView(latitude);
			// setContentView(longitude);
		} catch(Exception e) { }
	}
}
