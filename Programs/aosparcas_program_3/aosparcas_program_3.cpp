/*
Find the first unique character in a HUGE string.
Solve it in an optimized way.

Input: "axaaababawaax"
output: w
*/
#include <iostream>
#include <unordered_map>
using namespace std;

//    O(n²)
string uniqueChar(string arr) {
	int cont = 0;
	for (int i = 0; i < arr.length(); i ++) {
		for (int j = 0; j < arr.length(); j++) {
			if (arr[i] == arr[j]) {
				cont++;
			}
		}
		if (cont == 1) {
			return string(1, arr[i]);
		}
		cont = 0;
	}

	return "";
}

int main() {
	string v;
	cin >> v;
	cout << uniqueChar(v) << endl;
	return 0;
}