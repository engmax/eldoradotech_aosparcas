
#include <stdlib.h>

int main( int argc, char **argv, char **envp )
{
    char **env = NULL;

    for( env = envp; *env != 0; env++ )
    {
        char * variavel = *env;
        printf( "\n%s\n", variavel );
    }

    return 0;
}