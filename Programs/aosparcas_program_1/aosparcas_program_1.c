#include<stdio.h>
#include<stdlib.h>
void execCommand(char *com, char str[],char caracter);
void clearNumber(char num[]);
int number(char num);
int main()
{
   char process[200],mem[200],kernel[200],memUsed[200];

   execCommand("cat /proc/cpuinfo |grep 'model name' ",process,':');
   printf("Processor:%s",process);
   execCommand("grep MemTotal /proc/meminfo|awk '{print $2}'",mem,' ');
   printf("Total Memory:%s",mem);
   execCommand("uname -a",kernel,' ');
   printf("Kernel Version: %s",kernel);
   execCommand("free | grep Mem | awk '{print $3/$2 * 100.0}'",memUsed,' ');
   clearNumber(memUsed);
   printf("Memory Used:%s \n",memUsed);
   printf("Made By Luiz Nagata\n");
}

void execCommand(char *com, char str[],char caracter)
{
   FILE * fptr;                    // file holder
       char c;  // char buffer
       int index=0,achou=0;


       //system(com);      // call dir and put it's contents in a temp using redirects.
       fptr = popen(com,"r");  // open said file for reading.
                                       // oh, and check for fptr being NULL.
       while(1){
             if((caracter!=' ')&(achou==0))
               {
                  while(c!=caracter)
                    c=fgetc(fptr);
                  achou = 1;
                  c=fgetc(fptr);
               }
             else
               {
                  c=fgetc(fptr);
               }
               if(c== EOF)
                   break;                  // exit when you hit the end of the file.
               str[index] = c;
               index++;
       }


        fclose(fptr);                   // don't call this is fptr is NULL.
        //remove("temp.txt");             // clean up

}

void clearNumber(char num[])
{
   int indice=0;
   for(;((num[indice]=='.')||(number(num[indice])));indice++);
   num[indice]='\0';
}

int number(char num)
{
   if(num=='0') return 1;
   if(num=='1') return 1;
   if(num=='2') return 1;
   if(num=='3') return 1;
   if(num=='4') return 1;
   if(num=='5') return 1;
   if(num=='6') return 1;
   if(num=='7') return 1;
   if(num=='8') return 1;
   if(num=='9') return 1;
   return 0;
}