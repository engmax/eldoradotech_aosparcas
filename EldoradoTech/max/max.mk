$(call inherit-product, $(SRC_TARGET_DIR)/product/aosp_x86_64.mk)

PRODUCT_NAME := max
PRODUCT_DEVICE := max
PRODUCT_BRAND := Android
PRODUCT_MODEL  := AOSP Max Emulator


RODUCT_PACKAGES += \
LiveWallpapers \
Gallery2 \
SoundRecorder \
Camera \
Email \
FSLOta \
CactusPlayer \
VideoEditor \
FSLProfileApp \
FSLProfileService \
PinyinIME

PRODUCT_COPY_FILES += \
device/eldorado/max/init.rc:root/init.ranchu.rc	\
device/eldorado/max/vold.fstab:system/etc/vold.fstab	\
$(LOCAL_PATH)/gpsreset.sh:$(TARGET_COPY_OUT_SYSTEM)/etc/gpsreset.sh
